# chinochat

*based on 'chinochat' created by user [prempaolo](https://github.com/prempaolo), which can be found at https://github.com/prempaolo/chinochat*

*setup for Android 8+ by Andrea Arighi [andrea@chino.io] - https://github.com/aarighi - https://github.com/salvioner *

This is a sample application which uses [Chino.io](https://chino.io) as database as a service for the secure storage of sensitive health data.
The purpose of this app communication between

[https://github.com/tentello/chino-notifications](https://github.com/tentello/chino-notifications)

# Notes
**Build with Gradle 2.3.3**