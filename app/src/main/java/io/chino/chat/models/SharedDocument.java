package io.chino.chat.models;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;

public class SharedDocument {
    private final static SimpleDateFormat VIEW_DATE = new SimpleDateFormat("MMM dd yyyy - HH:mm", Locale.getDefault());

    private String datetime;
    private String doctorName;
    private String title;
    private String text;

    private String documentId;

    /**
     * Create a new ChinoShare {@link SharedDocument SharedDocument} from a {@link io.chino.api.document.Document Document} object of the Chino.io API
     * @param chinoDocument a {@link io.chino.api.document.Document} instance, returned by API calls to Chino.io
     */
    public SharedDocument(io.chino.api.document.Document chinoDocument) {
        datetime = VIEW_DATE.format(chinoDocument.getLastUpdate());

        HashMap<String, Object> docContent = chinoDocument.getContentAsHashMap();

        doctorName = (String) docContent.get("doctor");
        title = (String) docContent.get("title");
        text = (String) docContent.get("content");

        documentId = chinoDocument.getDocumentId();
    }

    public String getDatetimeString() {
        return datetime;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public String getChinoDocumentId() {
        return documentId;
    }
}
