package io.chino.chat.views;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import io.chino.R;
import io.chino.api.common.ChinoApiException;
import io.chino.chat.utils.Constants;

public class DocumentViewActivity extends AppCompatActivity {

    /* UI */
    TextView tbTitle;
    TextView docTitle, date, doctor, content;
    Button deleteBtn;
    ProgressBar progressBar;

    /* Others */
    private String chinoDocumentID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_document_details);

        tbTitle = (TextView) findViewById(R.id.customToolbarTitle);
        tbTitle.setText("Read Document");

        docTitle = (TextView) findViewById(R.id.viewDoc_title);
        date = (TextView) findViewById(R.id.viewDoc_date);
        doctor = (TextView) findViewById(R.id.viewDoc_doctor);
        content = (TextView) findViewById(R.id.viewDoc_text);

        Intent caller = getIntent();
        chinoDocumentID = caller.getStringExtra("id");
        docTitle.setText(caller.getStringExtra("title"));
        date.setText(caller.getStringExtra("date"));
        doctor.setText("created by " + caller.getStringExtra("doctor"));
        content.setText(caller.getStringExtra("content"));

        boolean canDelete = caller.getBooleanExtra("canDelete", false);
        deleteBtn = (Button) findViewById(R.id.viewDoc_delete);
        if (canDelete) {
            deleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new DeleteDocumentTask().execute();
                }
            });
        } else {
            deleteBtn.setVisibility(View.GONE);
        }

        progressBar = (ProgressBar) findViewById(R.id.viewDoc_deleteProgressBar);
    }

    private void setButtonsActive(boolean isActive) {
        deleteBtn.setActivated(isActive);

    }

    private class DeleteDocumentTask extends AsyncTask<Void, Void, Void> {

        String msg;

        @Override
        protected void onPreExecute() {
            setButtonsActive(false);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try{
                Constants.chino.documents.delete(chinoDocumentID, true);
                msg = "success";
            } catch (ChinoApiException e) {
                msg = "Server said: " + e.getMessage().replace("error, ", "");
            } catch (IOException e) {
                msg = "ERROR: " + e.getMessage();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (msg.equals("success")) {
                Toast.makeText(getApplicationContext(), "Document deleted", Toast.LENGTH_SHORT).show();
                finish();
            } else {
                deleteBtn.setText("Error - click to retry.");
            }
        }
    }
}
