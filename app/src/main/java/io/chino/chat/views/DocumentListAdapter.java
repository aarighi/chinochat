package io.chino.chat.views;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import io.chino.R;
import io.chino.chat.models.SharedDocument;

/**
 * Handles the visualization of {@link SharedDocument} objects on the {@link DocumentListActivity}
 * that sets it as their list adapter.
 *
 * @see RecyclerView.Adapter
 * @see RecyclerView#setAdapter(RecyclerView.Adapter)
 */
public class DocumentListAdapter extends RecyclerView.Adapter<DocumentListAdapter.ViewHolder> {

    /**
     *  {@link SharedDocument} dataset
     */
    private List<SharedDocument> docList;

    private final boolean isDoctor;

    /**
     * Initialize this Adapter with a dataset of {@link SharedDocument}.
     * @param dataset the list of SharedDocument objects to be displayed on the
     *                Document List.
     * @param parent the {@link DocumentListActivity} that is using this Adapter.
     */
    DocumentListAdapter(ArrayList<SharedDocument> dataset, DocumentListActivity parent) {
        docList = dataset;
        // determine who is watching this list, whether a Doctor or a Patient.
        isDoctor = (parent instanceof DoctorDocumentListActivity);
    }

    /**
     * Update the local dataset.
     * @param newDataset the new list of {@link SharedDocument} object to be displayed
     *                   on the Document List.
     */
    public void setDataset(Collection<SharedDocument> newDataset) {
        docList = new ArrayList<>(newDataset);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RelativeLayout docView = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.document_list_element, null);

        return new ViewHolder(docView, isDoctor);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SharedDocument currentDoc = docList.get(position);
        holder.setDocument(currentDoc);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return docList.size();
    }

    /**
     *  Implementation of {@link android.support.v7.widget.RecyclerView.ViewHolder RecyclerView.ViewHolder}
     *  to represent each item in the dataset on the Document List.
     */
    static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView title, doctorName, date;

        private final boolean isDoctor;

        public ViewHolder(RelativeLayout l, boolean isDoctor) {
            super(l);
            title = (TextView) this.itemView.findViewById(R.id.documentTitle);
            doctorName = (TextView) this.itemView.findViewById(R.id.doctorName);
            date = (TextView) this.itemView.findViewById(R.id.dateTime);

            this.isDoctor = isDoctor;
        }

        /**
         * Assigns a {@link SharedDocument} to this ViewHolder and maps the Document's
         * content to the fields of the layout.
         * @param document
         */
        public void setDocument(SharedDocument document) {
            title.setText(document.getTitle());
            title.post(new Runnable() {
                @Override
                public void run() {
                    Layout titleLayout = title.getLayout();
                    int ellipsis = titleLayout.getEllipsisStart(0);

                    if (ellipsis > 2) {
                        String shortTitle = title.getText().toString().substring(0, ellipsis - 3) + "...";
                        title.setText(shortTitle);
                    } else{
                        int newLine = title.getText().toString().indexOf("\n");
                        if (newLine >= 0)
                            title.setText(title.getText().toString().substring(0, newLine) + "...");
                    }
                }
            });
            doctorName.setText("by " + document.getDoctorName());
            date.setText(document.getDatetimeString());

            itemView.setOnClickListener(new OpenDocumentListener(document, isDoctor));
        }
    }

    /**
     *  standard {@link android.view.View.OnClickListener OnClickListener} for instances
     *  of {@link ViewHolder}
     */
    private static class OpenDocumentListener implements View.OnClickListener {

        private final SharedDocument doc;
        private final boolean isDoctor;

        /**
         * Create a new listener that will open the specified {@link SharedDocument}
         * @param sharedDocument the document to be opened.
         */
        public OpenDocumentListener(SharedDocument sharedDocument, boolean isDoctor) {
            doc = sharedDocument;
            this.isDoctor = isDoctor;
        }

        @Override
        public void onClick(View v) {
            Intent openDocumentView = new Intent(v.getContext(), DocumentViewActivity.class);

            openDocumentView.putExtra("id", doc.getChinoDocumentId());
            openDocumentView.putExtra("title", doc.getTitle());
            openDocumentView.putExtra("date", doc.getDatetimeString());
            openDocumentView.putExtra("doctor", doc.getDoctorName());
            openDocumentView.putExtra("content", doc.getText());

            openDocumentView.putExtra("canDelete", isDoctor);

            v.getContext().startActivity(openDocumentView);
        }
    }

}
