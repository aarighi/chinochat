package io.chino.chat.views;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

import io.chino.R;
import io.chino.api.common.ChinoApiException;
import io.chino.api.document.Document;
import io.chino.api.permission.PermissionRule;
import io.chino.api.user.User;
import io.chino.chat.utils.Constants;
import io.chino.java.ChinoAPI;

public class CreateDocumentActivity extends AppCompatActivity implements ChinoShareConsole {

    Toolbar toolbar;

    EditText documentTitle;
    ProgressBar progressBar;

    EditText patientEmail, documentContent;

    TextView console;

    Button shareBtn;

    DoctorDocumentListActivity doctorActivity;

    public CreateDocumentActivity() {
        // Check if user is a doctor
        boolean userHasAccess =  Constants.user.getAttributesAsHashMap().get("role").equals("doctor");

        if (! userHasAccess) {
            throw new IllegalArgumentException("Only doctors are allowed to create new Documents!");
        } else {
            doctorActivity = new DoctorDocumentListActivity();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_document_create);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        ((TextView) toolbar.findViewById(R.id.customToolbarTitle))
                .setText("New Document");

        documentTitle = (EditText) findViewById(R.id.createDoc_title);
        progressBar = (ProgressBar) findViewById(R.id.createDoc_progress);
        patientEmail = (EditText) findViewById(R.id.createDoc_patientID);

        documentContent = (EditText) findViewById(R.id.createDoc_content);

        console = (TextView) findViewById(R.id.createDoc_console);

        shareBtn = (Button) findViewById(R.id.createDoc_confirm);
        shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lockDocumentForSending(true);
                new DocumentCreationTask().execute();
            }
        });
    }

    public void lockDocumentForSending(boolean documentInactive) {
        showProgressBar(documentInactive);

        documentTitle.setActivated(! documentInactive);
        patientEmail.setActivated(! documentInactive);
        documentContent.setActivated(! documentInactive);

        shareBtn.setActivated(! documentInactive);
    }

    private void showProgressBar(boolean isShown) {
        int visibility = (isShown ? View.VISIBLE : View.GONE);
        progressBar.setVisibility(visibility);
    }

    @Override
    public void updateConsole(String text, Mode mode) {
        console.setText(text);
        switch (mode) {
            case HIDE:
                console.setVisibility(View.GONE);
                break;
            case ERR:
                console.setTextColor(Color.parseColor("#707070"));
                console.setVisibility(View.VISIBLE);
                break;
            case MSG:
                console.setTextColor(Color.LTGRAY);
                console.setVisibility(View.VISIBLE);
                break;
        }
    }


    private class DocumentCreationTask extends AsyncTask<Void, Void, Void> {

        String title, patientUserName, content;

        String msg;
        Mode mode;

        Document newDoc;
        User allowedUser = null;

        @Override
        protected void onPreExecute() {
            msg = null;
            mode = Mode.HIDE;


            title = documentTitle.getText().toString();
            patientUserName = patientEmail.getText().toString();
            content = documentContent.getText().toString();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                if (title.isEmpty()) {
                    throw new IOException("Please set a title first.");
                }

                // find allowed user
                LinkedList<User> users = new LinkedList<>();
                users.addAll(Constants.chino.users.list(Constants.PATIENT_USER_SCHEMA_ID).getUsers());
                users.addAll(Constants.chino.users.list(Constants.DOCTOR_USER_SCHEMA_ID).getUsers());
                for (User u : users) {
                    if (u.getUsername().equals(patientUserName)) {
                        allowedUser = u;
                        break;
                    }
                }

                if (allowedUser == null) {
                    throw new IOException("User '" + patientUserName + "' not found.");
                } else {
                    // create Document on Chino.io
                    newDoc = createDoc();

                    // Grant read permission on the document only if the user is a patient (doctors already have this permission
                    String allowedUserRole = (String) allowedUser.getAttributesAsHashMap().get("role");
                    if(allowedUserRole.equals("patient")) {

                        /* * * NOTE: this ChinoAPI uses Customer Credentials. This should not be done in production and should be detected as an error.  * * */
                        ChinoAPI permissionSetter = new ChinoAPI(Constants.HOST, Constants.CUSTOMER_ID, Constants.CUSTOMER_KEY);

                        PermissionRule readNewDoc = new PermissionRule();
                        readNewDoc.setManage("R");
                        permissionSetter.permissions.permissionsOnaResource("grant",
                                "documents", newDoc.getDocumentId(),
                                "users", allowedUser.getUserId(),
                                readNewDoc);
                    }
                }

            } catch (ChinoApiException e) {
                Logger.getLogger(getClass().getCanonicalName()).log(Level.SEVERE, e.getMessage());
                msg = "Server said: " + e.getMessage();
                mode = Mode.ERR;
            } catch (IOException e) {
                Logger.getLogger(getClass().getCanonicalName()).log(Level.SEVERE, e.getMessage());
                msg = "ERROR: " + e.getMessage();
                mode = Mode.ERR;
            }

            return null;
        }

        private Document createDoc() throws IOException, ChinoApiException {
            // create new Document
            HashMap<String, String> values = new HashMap<>();
            values.put("title", title);
            values.put("content", content);
            String doctorName = Constants.user.getAttributesAsHashMap().get("name") + " " + Constants.user.getAttributesAsHashMap().get("last_name");
            values.put("doctor", doctorName);
            return Constants.chino.documents.create(Constants.DOCUMENTS_SCHEMA_ID, values);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            lockDocumentForSending(false);
            if (mode == Mode.HIDE)
                finish(); // everything OK, return to Documents List
            else {
                // show error message
                updateConsole(msg, mode);
            }
        }
    }
}
