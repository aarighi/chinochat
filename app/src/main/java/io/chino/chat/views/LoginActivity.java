package io.chino.chat.views;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.IOException;

import io.chino.R;
import io.chino.api.auth.LoggedUser;
import io.chino.api.common.ChinoApiException;
import io.chino.chat.utils.Constants;
import io.chino.java.ChinoAPI;

import static android.graphics.Color.BLACK;
import static android.graphics.Color.RED;

public class LoginActivity extends AppCompatActivity implements ChinoShareConsole {

    private final static String HOST = Constants.HOST;

    TextView console;
    ProgressBar progressBar;
    EditText username;
    EditText password;
    Button login;
    Button register;
    Toolbar toolbar;
    TextView websiteLink;

    static String USERNAME;
    static String PASSWORD;
    static String USER_ID;

    public static SharedPreferences SETTINGS;

    public static String getUSERNAME() {
        return USERNAME;
    }

    public static String getPASSWORD() {
        return PASSWORD;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Constants.chino = new ChinoAPI(HOST);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        progressBar = (ProgressBar) findViewById(R.id.login_progress_bar);
        console = (TextView) findViewById(R.id.loginConsole);
        login = (Button) findViewById(R.id.login_user);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);

        register = (Button) findViewById(R.id.register_button);

        SETTINGS = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        if (SETTINGS.getBoolean(Constants.IS_LOGGED, false)) {
            // load user data from previous session
            loginUser(SETTINGS.getString(Constants.USERNAME, ""), SETTINGS.getString(Constants.PASSWORD, ""));
        }

        // Event handlers
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                View v = getCurrentFocus();
                if (v != null) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
                loginUser(username.getText().toString(), password.getText().toString());
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegistrationActivity.class);
                startActivity(intent);
            }
        });

        websiteLink = (TextView) findViewById(R.id.chino_io_website);
        websiteLink.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public void loginUser(String usernameValue, String passwordValue) {
        USERNAME = usernameValue;
        PASSWORD = passwordValue;
        new LoginAPITask(this).execute();
    }

    @Override
    public void updateConsole(String text, Mode mode) {
        console.setText(text);
        switch (mode) {
            case HIDE:
                console.setVisibility(View.GONE);
                break;
            case ERR:
                console.setTextColor(RED);
                console.setVisibility(View.VISIBLE);
                break;
            case MSG:
                console.setTextColor(BLACK);
                console.setVisibility(View.VISIBLE);
                break;
        }
    }

    public void showProgressBar(boolean isVisible) {
        if (isVisible) {
            this.progressBar.setVisibility(View.VISIBLE);
        } else {
            this.progressBar.setVisibility(View.GONE);
        }
    }

    public void showLoginFields(boolean fieldsAreVisible) {
        int visibility = (fieldsAreVisible ? View.VISIBLE : View.GONE);

        showProgressBar(!fieldsAreVisible);
        login.setVisibility(visibility);
        register.setVisibility(visibility);
        username.setVisibility(visibility);
        password.setVisibility(visibility);
    }

    private class LoginAPITask extends AsyncTask<Void, Void, String> {

        private String action;
        private LoginActivity originActivity;

        LoginAPITask(LoginActivity activity) {
            super();
            originActivity = activity;
        }

        @Override
        protected void onPreExecute() {
            originActivity.showLoginFields(false);
            originActivity.updateConsole("Checking user credentials...", Mode.MSG);
        }

        @Override
        protected String doInBackground(Void... voids) {

            // log in to Chino.io
            try {

                LoggedUser loggedUser = Constants.chino.auth.loginWithPassword(USERNAME, PASSWORD, Constants.APPLICATION_ID, Constants.APPLICATION_SECRET);

                Constants.A_TOKEN = loggedUser.getAccessToken();
                Constants.R_TOKEN = loggedUser.getRefreshToken();

                Constants.user = Constants.chino.auth.checkUserStatus();

                if (Constants.user.getAttributesAsHashMap().get("role").equals("doctor")) {
                    USER_ID = Constants.user.getUserId();
                    Log.d("API", "doctor " + USER_ID);
                    return "doctor";
                } else if (Constants.user.getAttributesAsHashMap().get("role").equals("patient")) {
                    USER_ID = Constants.user.getUserId();
                    Log.d("API", "patient " + USER_ID);
                    return "patient";
                } else {
                    return "error - invalid role";
                }
            } catch (ChinoApiException e) {
                return "Server said: " + e.getMessage().replace("error, ", "");
            } catch (IOException e) {
                return "ERROR: " + e.getMessage();
            }
        }


        @Override
        protected void onPostExecute(String msg) {
            SharedPreferences.Editor editor = LoginActivity.SETTINGS.edit();

            Intent nextView = null;

            // after LOGIN
            if (msg.equals("patient") | msg.equals("doctor")) {

                originActivity.updateConsole("Logged in as " + msg, Mode.MSG);

                // Save Username/Password to SharedPreference, which persist even if app is stopped.
                editor.putString(Constants.USERNAME, USERNAME);
                editor.putString(Constants.PASSWORD, PASSWORD);
                editor.putString(Constants.USER_ID, USER_ID);

                if (msg.equals("patient")) {
                    editor.putBoolean(Constants.IS_DOCTOR, false);
                    nextView = new Intent(LoginActivity.this, DocumentListActivity.class);
                } else {
                    editor.putBoolean(Constants.IS_DOCTOR, true);
                    nextView = new Intent(LoginActivity.this, DoctorDocumentListActivity.class);
                }

                editor.putBoolean(Constants.IS_LOGGED, true);
                editor.apply();

                originActivity.updateConsole("", Mode.HIDE);
            } else {
                originActivity.updateConsole(msg, Mode.ERR);
            }
            // restore LoginActivity View
            originActivity.showLoginFields(true);

            if (nextView != null)
                startActivity(nextView);
        }
    }
}
