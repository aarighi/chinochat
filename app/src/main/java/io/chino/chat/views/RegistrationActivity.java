package io.chino.chat.views;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.HashMap;

import io.chino.R;
import io.chino.api.common.ChinoApiException;
import io.chino.api.permission.PermissionRule;
import io.chino.api.user.User;
import io.chino.chat.utils.Constants;
import io.chino.java.ChinoAPI;

public class RegistrationActivity extends AppCompatActivity {

    //They simulate a server
    private final static String CUSTOMER_ID = Constants.CUSTOMER_ID;
    private final static String CUSTOMER_KEY = Constants.CUSTOMER_KEY;
    String NAME;
    String LAST_NAME;
    String EMAIL;
    String PASSWORD;

    Toolbar toolbar;

    EditText name;
    EditText lastName;
    EditText email;

    EditText password, repeatPassword;

    CheckBox privacyAccept;
    TextView privacyLink;

    Button addPatientBtn;
    TextView outcome;

    Button reset;

    /**
     * Check if required Chino.io objects exist before usage.
     * If not, throws {@link NullPointerException}
     */
    private void requireObjects(String constant) throws NullPointerException {
        if (constant == null) {
            throw new NullPointerException("required constant is null.");
        }

        if (constant.isEmpty()) {
            throw new NullPointerException("required constant is an empty String.");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        Constants.chino = new ChinoAPI(Constants.HOST);

        toolbar = (Toolbar)findViewById(R.id.tool_bar);
        TextView tbText = (TextView) toolbar.findViewById(R.id.customToolbarTitle);
        tbText.setText("New Account");
        setSupportActionBar(toolbar);

        name = (EditText)findViewById(R.id.register_first_name);
        lastName = (EditText)findViewById(R.id.register_last_name);
        email = (EditText)findViewById(R.id.register_email);

        password = (EditText)findViewById(R.id.register_password);
        repeatPassword = (EditText)findViewById(R.id.register_repeat_password);

        privacyAccept = (CheckBox) findViewById(R.id.privacyPolicyCB);
        privacyLink = (TextView) findViewById(R.id.privacyPolicyLink);
        privacyLink.setMovementMethod(LinkMovementMethod.getInstance());

        addPatientBtn = (Button)findViewById(R.id.add_patient);
        addPatientBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDataAndCreate();

            }
        });

        outcome = (TextView) findViewById(R.id.registrationConsole);
    }


    private void checkDataAndCreate() {
        // handle user input errors
        if (password.getText()==null || password.getText().toString().equals("")) {
            Toast.makeText(RegistrationActivity.this, "Please insert a valid password (8+ characters).", Toast.LENGTH_SHORT).show();
            return;
        } else if (password.getText().toString().length() < 8) {
            Toast.makeText(RegistrationActivity.this, "Password must be at least 8 characters.", Toast.LENGTH_SHORT).show();
            return;
        } else if (!password.equals(repeatPassword)) {
            Toast.makeText(RegistrationActivity.this, "Passwords don't match.", Toast.LENGTH_SHORT).show();
            return;
        } else if(name.getText()==null || name.getText().toString().equals("") ||
                lastName.getText()==null || lastName.getText().toString().equals("") ||
                email.getText()==null || email.getText().toString().equals("")) {
            Toast.makeText(RegistrationActivity.this, "Missing data, please fill all the fields!", Toast.LENGTH_SHORT).show();
            return;
        } else if (!privacyAccept.isChecked()) {
            Toast.makeText(RegistrationActivity.this, "You need to accept the Privacy Policy first!", Toast.LENGTH_SHORT).show();
            return;
        } else {
            // if data are correct, create user.
            new UserCreationTask().execute("patient");
        }
    }

    private class UserCreationTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            NAME = name.getText().toString();
            LAST_NAME = lastName.getText().toString();
            EMAIL = email.getText().toString();
            PASSWORD = password.getText().toString();
            //STE: disable button and showing a toast.
            addPatientBtn.setEnabled(false);
            Toast.makeText(RegistrationActivity.this, "Sending data for registration...", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                createUser(RegistrationActivity.this, NAME, LAST_NAME, EMAIL, PASSWORD, params[0], Constants.PATIENT_USER_SCHEMA_ID);
                return Constants.SUCCESS;
            } catch (ChinoApiException e) {
                return "Server said: " + e.getMessage().replace("error, ", "");
            } catch (IOException e) {
                return "ERROR: " + e.getMessage();
            }
        }


        @Override
        protected void onPostExecute(String result){
            if(result.equals(Constants.SUCCESS)) {
                Toast.makeText(RegistrationActivity.this, "User created successfully!", Toast.LENGTH_SHORT).show();
                finish();
            } else {
                Toast.makeText(RegistrationActivity.this, result, Toast.LENGTH_SHORT).show();
            }
            addPatientBtn.setEnabled(true);

        }


        private void givePermission(User user) throws IOException, ChinoApiException {

            /* * * NOTE: this ChinoAPI uses Customer Credentials. This should not be done in production and should be detected as an error.  * * */
            ChinoAPI customerClient = new ChinoAPI(Constants.HOST, Constants.CUSTOMER_ID, Constants.CUSTOMER_KEY);

            PermissionRule listPermission = new PermissionRule();
            // Patient Permissions
            listPermission.setManage("L");
            customerClient.permissions.permissionsOnResourceChildren("grant",
                    "schemas", Constants.DOCUMENTS_SCHEMA_ID,
                    "documents",
                    "users", user.getUserId(),
                    listPermission);
        }

        void createUser(RegistrationActivity registrationActivity, String name, String lastName, String email, String password, String role, String userSchemaId) throws IOException, ChinoApiException {

            /* * * NOTE: this ChinoAPI uses Customer Credentials. This should not be done in production and should be detected as an error.  * * */
            ChinoAPI chinoTemp = new ChinoAPI(Constants.HOST, CUSTOMER_ID, CUSTOMER_KEY);

            HashMap<String, Object> attributes = new HashMap<>();
            attributes.put("role", role);
            attributes.put("tokenFCM", "");
            attributes.put("name", name);
            attributes.put("last_name", lastName);

            User user = chinoTemp.users.create(email, password, attributes, userSchemaId);
            givePermission(user);
        }

    }

}

