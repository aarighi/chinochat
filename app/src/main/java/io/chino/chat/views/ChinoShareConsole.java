package io.chino.chat.views;

public interface ChinoShareConsole {

    enum Mode {
        /**
         * Use this Mode to display normal info messages
         */
        MSG,
        /**
         * Use this Mode to display error message and exceptions
         */
        ERR,
        /**
         * Use this mode to hide the console from the view.
         */
        HIDE}

    /**
     * Change the text of this interface's console
     * according to the specified {@link Mode}.
     * @param text the new text
     * @param mode one of {@link Mode#ERR ERR}, {@link Mode#MSG MSG}
     *             and {@link Mode#HIDE HIDE}.
     */
    void updateConsole(String text, Mode mode);


}
