package io.chino.chat.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;

import java.io.IOException;

import io.chino.R;
import io.chino.api.common.ChinoApiException;
import io.chino.api.permission.PermissionRule;
import io.chino.chat.utils.Constants;
import io.chino.java.ChinoAPI;

public class DoctorDocumentListActivity extends DocumentListActivity {

    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fab = (FloatingActionButton) findViewById(R.id.documentListFab);
        fab.setVisibility(View.VISIBLE);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View thisFab) {
                Intent intent = new Intent(DoctorDocumentListActivity.this, CreateDocumentActivity.class);
                startActivity(intent);
            }
        });
    }

}
