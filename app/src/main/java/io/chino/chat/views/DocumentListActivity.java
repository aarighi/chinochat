package io.chino.chat.views;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import io.chino.R;
import io.chino.api.common.ChinoApiException;
import io.chino.api.document.Document;
import io.chino.chat.models.SharedDocument;
import io.chino.chat.utils.Constants;
import io.chino.java.ChinoAPI;

public class DocumentListActivity extends AppCompatActivity implements ChinoShareConsole {

    /* UI */
    private RecyclerView documentListView;
    private DocumentListAdapter documentListAdapter;
    private RecyclerView.LayoutManager documentListLayoutManager;
    private TextView console;
    private TextView logoutBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_document_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);

        String activityTitle;
        if (Constants.user != null) {
            activityTitle = "Hello, " + Constants.user.getAttributesAsHashMap().get("name");
        } else {
            activityTitle = "Documents";
        }
        ((TextView) toolbar.findViewById(R.id.customToolbarTitle))
                .setText(activityTitle);
        setSupportActionBar(toolbar);


        /* fetch UI Elements */
        console = (TextView) findViewById(R.id.documentsConsole);
        // init documents docList
        documentListView = (RecyclerView) findViewById(R.id.document_list);
        documentListView.setHasFixedSize(true);

        // set layout
        documentListLayoutManager = new LinearLayoutManager(getApplicationContext());
        documentListView.setLayoutManager(documentListLayoutManager);

        // set adapter
        documentListAdapter = new DocumentListAdapter(new ArrayList<SharedDocument>(), this);
        documentListView.setAdapter(documentListAdapter);
        refresh();

        // logout button
        final DocumentListActivity thisActivity = this;
        logoutBtn = (TextView) findViewById(R.id.logout_btn);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogoutAPITask logoutTask = new LogoutAPITask(thisActivity);
                logoutTask.execute();
            }
        });
        logoutBtn.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.refresh();
    }

    /**
     * Fetch {@link Document Documents} from Chino.io and maps them to a docList of {@link SharedDocument}.
     *
     * @return the docList of fetched {@link Document Documents}.
     */
    protected void refresh() {
        List<Document> docs = new LinkedList<>();


        updateConsole("", Mode.HIDE);
        new FetchDocs().execute(docs);
    }

    @Override
    public void updateConsole(String text, Mode mode) {
        console.setText(text);
        switch (mode) {
            case HIDE:
                console.setVisibility(View.GONE);
                break;
            case ERR:
                console.setTextColor(Color.parseColor("#707070"));
                console.setVisibility(View.VISIBLE);
                break;
            case MSG:
                console.setTextColor(Color.LTGRAY);
                console.setVisibility(View.VISIBLE);
                break;
        }
    }

    /** Async tasks */

    private class LogoutAPITask extends AsyncTask<Void, Void, String> {

        private final DocumentListActivity docListActivity;

        private LogoutAPITask(DocumentListActivity docListActivity) {
            this.docListActivity = docListActivity;
        }

        @Override
        protected void onPreExecute() {
            SharedPreferences.Editor editor = LoginActivity.SETTINGS.edit();
            editor.putBoolean(Constants.IS_LOGGED, false);
            editor.apply();

            updateConsole("Log out...", Mode.MSG);
        }

        @Override
        protected String doInBackground(Void... voids) {
            if (Constants.A_TOKEN != null && Constants.R_TOKEN != null) {
                // log out from Chino.io
                try {
                    Constants.chino.auth.logout(Constants.A_TOKEN);
                } catch (ChinoApiException e) {
                    return "Server said: " + e.getMessage().replace("error, ", "");
                } catch (IOException e) {
                    return "ERROR: " + e.getMessage();
                }
            }
            return "success";
        }

        @Override
        protected void onPostExecute(String msg) {
            // after LOGOUT
            Constants.A_TOKEN = null;
            Constants.R_TOKEN = null;
            SharedPreferences.Editor editor = LoginActivity.SETTINGS.edit();
            editor.putString(Constants.USERNAME, null);
            editor.putString(Constants.PASSWORD, null);
            editor.putString(Constants.USER_ID, null);
            editor.putBoolean(Constants.IS_LOGGED, false);
            editor.apply();


            Mode mode = Mode.HIDE;

            // back to login activity
            docListActivity.finish();
        }
    }

    private class FetchDocs extends AsyncTask<List<Document>, Void, Integer> {

        private List<Document> docList;

        private String errorMessage;

        @Override
        protected void onPreExecute() {
            updateConsole("Refreshing Documents...", Mode.MSG);
        }

        @Override
        protected Integer doInBackground(List<Document> ...lists) {
            try {
                docList = lists[0];
                if (Constants.chino == null) {
                    Constants.chino = new ChinoAPI(Constants.HOST);
                    Constants.chino.auth.loginWithPassword(LoginActivity.getUSERNAME(), LoginActivity.getPASSWORD(), Constants.APPLICATION_ID, Constants.APPLICATION_SECRET);
                }

                docList.addAll(Constants.chino.documents.list(Constants.DOCUMENTS_SCHEMA_ID, true).getDocuments());
            } catch (ChinoApiException e) {
                errorMessage = "Server said: " + e.getMessage().replace("error, ", "");
            } catch (IOException e) {
                errorMessage =  "ERROR: " + e.getMessage();
            }
            return docList.size();
        }

        @Override
        protected void onPostExecute(Integer expectedResults) {
            ArrayList<SharedDocument> sharedDocumentsList = new ArrayList<>();
            Mode mode = Mode.MSG;
            String msg = "";

            // map results to a chatDocumentList
            if (expectedResults >= 0) {
                for (Document doc : docList) {
                    sharedDocumentsList.add(new SharedDocument(doc));
                }
            }

            // update dataset and RecyclerView
            documentListAdapter.setDataset(sharedDocumentsList);
            documentListAdapter.notifyDataSetChanged();

            // update console status
            if (expectedResults < 0) {
                msg = errorMessage;
            } else if (documentListAdapter.getItemCount() == expectedResults) {
                msg = expectedResults + " Documents found.";
            } else {
                msg = "Only " +
                        documentListAdapter.getItemCount() + "/" + expectedResults +
                        " Documents found.";
                mode = Mode.ERR;
            }

            updateConsole(msg, mode);
        }
    }

}
