package io.chino.chat.utils;

import io.chino.api.user.User;
import io.chino.java.ChinoAPI;

// FBK Constants
public class Constants {

    // Set these constants with your own values

    /**
     *  Chino.io API base URL
     */
    public final static String HOST = "https://api.test.chino.io/v1";

    /**
     * Application ID (create a Chino.io Application on https://console.test.chino.io)
     */
    public static String APPLICATION_ID = // sensitive
            "BHuXUBL5HUt0KQufzabA4ZCvwqnUN5zxKc7257ES";

    /**
     *  Application secret (create a Chino.io Application on https://console.test.chino.io)
     */
    public static String APPLICATION_SECRET = // sensitive
            "7NVAO9bG0JMnpd9hDFRVCAgoJsOWqxgOlG8VsMSlzVRDNpKwskQpZtFXJvrrfUhBdSgmJ20uUc5poWRqiLZeerLNbSN9Sw80ybm5QALjcWB2YdcHMIXEKGxiGHZJ8W6u";

    /**
     *  Chino.io sandbox console customer_id
     */
    public static String CUSTOMER_ID = // sensitive
            "9a1b07b3-a5f3-4fce-8e29-daf1627b25bb";

    /**
     *  Chino.io sandbox console customer_key
     */
    public static String CUSTOMER_KEY = // sensitive
            "ec953845-6d5f-4810-beeb-e38cdf0fef4e";


    /**
     * ID of the Schema where we store Documents to be shared between patient and doctor.
     * If the Schema has not already been created, you can create a new one at
     * <a href="https://console.test.chino.io">https://console.test.chino.io</a>. <br>
     * <br>
     * It must contain the following fields: <br>
     * <code> <br>
     *     "doctor": "string", indexed <br>
     *     "title": "string", indexed <br>
     *     "content": "text"
     * </code>
     */
    public static final String DOCUMENTS_SCHEMA_ID = "8ea1b35d-db4f-49fb-b10c-58a2977d66da";

    /** These values are automatically set at runtime: **/

    // Access tokens for user authentication (set at runtime)
    public static String A_TOKEN = "";
    public static String R_TOKEN = "";

    // This values are keys that are used to fetch
    // user's data from SharedPreferences at runtime:
    public static String USERNAME = "username";
    public static String PASSWORD = "password";
    public static String USER_ID = "user_id";
    public static String IS_DOCTOR = "is_doctor";
    public static String IS_LOGGED = "is_logged";

    /**
     * The main instance of ChinoAPI, created at runtime
     */
    public static ChinoAPI chino;

    /**
     * This value holds user data (set at runtime)
     */
    public static User user;




    /** Other constants **/

    public static String SUCCESS = "success";
    public static String FAIL = "fail";

    public static String DOCTOR_USER_SCHEMA_ID = "3aced77a-a5ac-4702-992c-f98e1bc973e1";
    public static String PATIENT_USER_SCHEMA_ID = "3b7a9773-8225-42d3-9ba3-c74ab4bf75f3";
    public static String CHAT_REPOSITORY_ID = "";
    public static String REPOSITORY_ID = "";
    public static String ROLE_SCHEMA_ID = "";
    public static String NOTIFICATIONS_SCHEMA_ID = "";
//    public static String USERS_GROUP_ID = "";
////    //public static String CHAT_SCHEMA_ID = "";
////    public static String DOCTORS = "doctors";
////    public static String PATIENTS = "patients";
////    public static String FROM_USER_TO_CHAT = "users";
    public static String MESSAGE_USERNAME = "username";
    public static String MESSAGE_CONTENT = "message";
    public static String MESSAGE_TIME= "time";
    public static String MESSAGE_DATE= "date";
    public static String MESSAGE_USER_ID = "user_id";
    public static String MESSAGE_ROLE = "role";
    public static String INTENT_ADD_DOCTOR_REQUEST = "io.chino.intent_added_doctor";
    public static String INTENT_NEW_MESSAGE = "io.chino.intent_new_message";
//    public static String INTENT_OPEN_CHAT = "io.chino.intent_open_chat";
//    public static String SERVER_KEY_DOCUMENT_ID = "";
}
